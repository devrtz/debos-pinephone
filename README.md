# debos-pinephone

A set of [debos](https://github.com/go-debos/debos) recipes for building a
debian-based image for the PinePhone.

Prebuilt images are available
[here](http://pinephone.a-wai.com/images/).

The default user is `debian` with password `1234`.
The default `root` password is `root`.

## Build

To build the image, you need to have `debos` and `bmaptool`. On a debian-based
system, install these dependencies by typing the following command in a terminal:

```
sudo apt install debos bmap-tools
```

Then simply browse to the `debos-pinephone` folder and execute `./build.sh`.

If your system isn't debian-based (or if you choose to install `debos` without
using `apt`, which is a terrible idea), please make sure you also install the
following required packages:
- `debootstrap`
- `qemu-system-x86`
- `qemu-user-static`
- `binfmt-support`

You can use `./build.sh -d` to use the docker version of `debos`.

## Install

Insert a MicroSD card into your computer, and type the following command:

```
sudo bmaptool copy debian-pinephone.img /dev/<sdcard>
```

or:

```
sudo dd if=debian-pinephone.img of=/dev/<sdcard> bs=1M
```

*Note: Make sure to use your actual SD card device, such as `mmcblk0` instead of
`<sdcard>`.*

**CAUTION: This will format the SD card and erase all its contents!!!**

## What works, what doesn't

The following features work, at least most of the time:

- UART console
- touchscreen
- graphics acceleration
- wifi connection
- web browsing
- text messaging
- sound playback
- voice calls
- bluetooth

However, keep in mind this is a developer preview, and a number of features are
not yet implemented:

- power management
- camera support

## Third-party software

The project's Wiki provides
[a list](https://gitlab.com/a-wai/debos-pinephone/-/wikis/software) of software
packages modified or created to work on the PinePhone.

## Contributing

If you want to help with this project, please have a look at the
[TODO list](https://gitlab.com/a-wai/debos-pinephone/-/wikis/development) and
[open issues](https://gitlab.com/a-wai/debos-pinephone/-/issues).

In case you need more information, feel free to get in touch with the developers on
the Pine64 [forum](https://forum.pine64.org/showthread.php?tid=9016).

# License

This software is licensed under the terms of the GNU General Public License,
version 3.
